<?php

$router->get('article', function ()  {
    return view('index');
});
// List articles
$router->group(['prefix' => 'api'], function () use ($router) {
	// Get Articles
	$router->get('/article', 'ArticleController@index');
	
	// List single article
	$router->get('/article/{id}', 'ArticleController@show');

	//Create new article
	$router->post('/article', 'ArticleController@store');

	// Update article
	$router->post('/article/{id}', 'ArticleController@update');

	// Delete article
	$router->delete('/article/delete/{id}', 'ArticleController@destroy');

	// Get Categories
	$router->get('/category', 'CategoryController@index');
	// Get Categories
	$router->post('/category', 'CategoryController@store');
});