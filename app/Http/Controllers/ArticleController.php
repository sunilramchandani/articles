<?php

namespace App\Http\Controllers;

use App\Interfaces\ArticleRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Article;
use Illuminate\Support\Facades\Validator;
use Exception;

class ArticleController extends Controller
{
    private $articleRepository;

    public function __construct(ArticleRepositoryInterface $articleRepository) 
    {
        $this->articleRepository = $articleRepository;
    }

    private function validateForm($data){
        $validator = Validator::make($data, [
            'title' => 'required|max:255',
            'description' => 'required',
            'category_id' => 'required',
        ]);
        return $validator;
    }
    
    public function index()
    {
        try {
            return response()->json([
                'data' => $this->articleRepository->getAllArticles()
            ]);
        }catch (Exception $e) {
            return response()->json([
                'error' => [
                    'description' => $e->getMessage()
                ]
            ], 500);
        }
    }

    public function store(Request $request): JsonResponse 
    {

        $articleDetails = $request->only([
            'title',
            'description',
            'category_id',
            //'image_path',
        ]);

        $validator = $this->validateForm($articleDetails);

        if ($validator->fails()) {
            return response()->json(['isvalid'=>false,'errors'=>$validator->messages()]);
        }else{
            return response()->json( ['data' => $this->articleRepository->createArticles($articleDetails)],Response::HTTP_CREATED);
        }
        
    }

    public function show(Request $request): JsonResponse 
    {
        $articleId = $request->route('id');

        return response()->json([
            'data' => $this->articleRepository->getArticlesById($articleId)
        ]);
    }

    public function update(Request $request): JsonResponse 
    {
        $articleId = $request->route('id');
        $articleDetails = $request->only([
            'title',
            'description',
            'category_id',
            //'image_path',
        ]);
        $validator = $this->validateForm($articleDetails);
        if ($validator->fails()) {
            return response()->json(['isvalid'=>false,'errors'=>$validator->messages()]);
        }else{
            return response()->json([
                'data' => $this->articleRepository->updateArticles($articleId, $articleDetails)
            ]);
        }

    }

    public function destroy(Request $request): JsonResponse 
    {
        $articleId = $request->route('id');
        $this->articleRepository->deleteArticles($articleId);
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
