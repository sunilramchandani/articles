<?php

namespace App\Http\Controllers;

use App\Interfaces\CategoryRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Article;

class CategoryController extends Controller
{
    private $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository) 
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        return response()->json([
            'data' => $this->categoryRepository->getAllCategories()
        ]);
    }

    public function store(Request $request): JsonResponse 
    {
        $categoryDetails = $request->only([
            'title',
            'description',
            'category_id',
            //'image_path',
        ]);

        return response()->json( ['data' => $this->categoryRepository->createCategories($categoryDetails)],Response::HTTP_CREATED);
    }

    public function show(Request $request): JsonResponse 
    {
        $categoryId = $request->route('id');

        return response()->json([
            'data' => $this->categoryRepository->getCategoriesById($categoryId)
        ]);
    }

    public function update(Request $request): JsonResponse 
    {
        $categoryId = $request->route('id');
        $categoryDetails = $request->only([
            'title',
            'description',
            'category_id',
            //'image_path',
        ]);

        return response()->json([
            'data' => $this->categoryRepository->updateCategories($categoryId, $categoryDetails)
        ]);
    }

    public function destroy(Request $request): JsonResponse 
    {
        $categoryId = $request->route('id');
        $this->categoryRepository->deleteCategories($categoryId);
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
