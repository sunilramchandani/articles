<?php

namespace App\Interfaces;

interface ArticleRepositoryInterface 
{
    public function getAllArticles();
    public function getArticlesById($articleId);
    public function deleteArticles($articleId);
    public function createArticles(array $articleDetails);
    public function updateArticles($articleId, array $newDetails);
}