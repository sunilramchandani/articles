<?php

namespace App\Interfaces;

interface CategoryRepositoryInterface 
{
    public function getAllCategories();
    public function getCategoriesById($categoryId);
    public function deleteCategories($categoryId);
    public function createCategories(array $categoryDetails);
    public function updateCategories($categoryId, array $newDetails);
}