<?php

namespace App\Repositories;

use App\Interfaces\ArticleRepositoryInterface;
use App\Models\Article;
use Carbon\Carbon;

class ArticleRepository implements ArticleRepositoryInterface 
{
    public function getAllArticles() 
    {
        return Article::where('deleted_at',NULL)->with('category')->get();
    }

    public function getArticlesById($articleId) 
    {
        return Article::where('id',$articleId)->with('category')->first();
    }

    public function deleteArticles($articleId) 
    {
        Article::whereId($articleId)->update(['deleted_at'=> Carbon::now()->toDateTimeString()]);
    }

    public function createArticles(array $articleDetails) 
    {
        return Article::create($articleDetails);
    }

    public function updateArticles($articleId, array $newDetails) 
    {
        return Article::whereId($articleId)->update($newDetails);
    }
}
