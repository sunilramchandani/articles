<?php

namespace App\Repositories;

use App\Interfaces\CategoryRepositoryInterface;
use App\Models\Category;
use Carbon\Carbon;

class CategoryRepository implements CategoryRepositoryInterface 
{
    public function getAllCategories() 
    {
        return Category::where('deleted_at',NULL)->get();
    }

    public function getCategoriesById($categoryId) 
    {
        return Category::findOrFail($categoryId);
    }

    public function deleteCategories($categoryId) 
    {
        Category::whereId($categoryId)->update(['deleted_at'=> Carbon::now()->toDateTimeString()]);
    }

    public function createCategories(array $categoryDetails) 
    {
        return Category::create($categoryDetails);
    }

    public function updateCategories($categoryId, array $newDetails) 
    {
        return Category::whereId($categoryId)->update($newDetails);
    }
}
