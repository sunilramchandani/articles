<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Log;

class Category extends Model
{
    use HasFactory;

    public function article(){
      return $this->belongsTo(Article::class,'category_id');
    }


}
