@extends('layout')

@section('content')
<div class="flex bg-gray-100 border-b border-gray-300 py-4">
    <div class="container mx-auto flex justify-between">
        <div class="flex">
            <router-link class="mr-4" to='/article' exact>Articles</router-link>
            <router-link class="mr-4" to='/category' exact>Categories</router-link>
        </div>
        <div class="flex">
           	<router-link class="mr-4" to='/article/new'>New Article</router-link>
            <router-link class="mr-4" to='/category/new'>New Category</router-link>
        </div>
    </div>
</div>
<router-view></router-view>
@endsection