import ArticleIndex from './components/articles/ArticleIndex';
import ArticleForm from './components/articles/ArticleForm';
import CategoryForm from './components/categories/CategoryForm';
import NotFound from './components/PageNotFound';

export default{
	mode: 'history',
	linkActiveClass: 'font-semibold',
	routes:[
		{
			path: '/article',
			name: "List",
			component: ArticleIndex
			
		},
		{
			path: '/article',
			name: "New Article",
			component: ArticleForm
		},
		{
			path: '/category/new',
			name: "New Category",
			component: CategoryForm
		},
		{
			path: '/article/:id',
			name: "Edit Article",
			component: ArticleForm
		},
		{ 	path: "*", 
			component: ArticleIndex 
		}
	]
}