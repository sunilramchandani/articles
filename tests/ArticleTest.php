<?php

class ArticleTest extends TestCase
{
    /**
     * /products [GET]
     */
    public function testShouldReturnAllArticles(){

        $this->get("api/article", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'data' => ['*' =>
                [
                    'title',
                    'category' => ['name'],
                    'description'
                ]
            ]
        ]);
        
    }

    /**
     * /products/id [GET]
     */
    public function testShouldReturnArticle(){
        $this->get("api/article/1", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'title',
                    'category' => ['name'],
                    'description'
                ]
            ]    
        );
        
    }

    /**
     * /products [POST]
     */
    public function testShouldCreateArticle(){

        $parameters = [
            'title' => 'Try Article Test',
            'category_id' => '1',
            'description' => 'I am trying unit testing',
        ];

        $this->post("api/article", $parameters, []);
        $this->seeStatusCode(201);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'title',
                    'category_id',
                    'description'
                ]
            ]    
        );
        
    }
    
    /**
     * /products/id [PUT]
     */
    public function testShouldUpdateArticle(){

        $parameters = [
            'title' => 'Try Article Test Updated',
            'category_id' => '1',
            'description' => 'I am trying unit testing Updated',
        ];

        $this->post("api/article/1", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'title',
                    'category_id',
                    'description'
                ]
            ]    
        );
    }

    /**
     * /products/id [DELETE]
     */
    public function testShouldDeleteArticle(){
        
        $this->delete("api/article/delete/1", [], []);
        $this->seeStatusCode(410);
        $this->seeJsonStructure([
            'message',
            'status'
        ]);
    }

}